#!/bin/bash

WGET=`which wget >/dev/null ; echo $?`
CURL=`which curl >/dev/null ; echo $?`

URL=$1
FILENAME=$2

if [ $WGET = "0" ]; then
	wget -P ./images $URL
	exit 0
fi

if [ $CURL = "0" ]; then
	curl -L $URL -o ./images/$FILENAME -s
	exit 0
fi

echo Error: Neither wget nor curl found on the system.
exit 1
