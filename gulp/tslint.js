const gulp = require("gulp");
const tslint = require("gulp-tslint");

module.exports = () => {
	const config = require("../tsconfig.json");
	return gulp.src(config.include)
		.pipe(tslint({
			configuration: "tslint.json",
			formatter: "prose"
		}))
		.pipe(tslint.report());
};