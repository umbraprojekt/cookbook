const childProcess = require("child_process");

module.exports.dist = (callback) => {
	childProcess.exec("node_modules/.bin/tsc --inlineSourceMap false --inlineSources false", (err, stdout, stderr) => {
		console.log(stdout);
		console.log(stderr);
		callback(err);
	});
};

module.exports.dev = (callback) => {
	childProcess.exec("node_modules/.bin/tsc", (err, stdout, stderr) => {
		console.log(stdout);
		console.log(stderr);
		callback(err);
	});
};