const gulp = require("gulp");
const imagemin = require("gulp-imagemin");
const jimp = require("gulp-jimp");
const childProcess = require("child_process");
const fs = require("fs");
const path = require("path");
const clean = require("gulp-clean");

const jimpProcessing = jimp({
	"-list": {
		cover: { width: 240, height: 180 },
		type: "jpg"
	},
	"-list-2x": {
		cover: { width: 480, height: 360 },
		type: "jpg"
	}
});

module.exports.dev = () => {
	return gulp.src("images/**/*.{png,jpg,jpeg}")
		.pipe(jimpProcessing)
		.pipe(gulp.dest("public/img"));
};

module.exports.dist = () => {
	return gulp.src("images/**/*.{png,jpg,jpeg}")
		.pipe(jimpProcessing)
		.pipe(imagemin())
		.pipe(gulp.dest("public/img"));
};

const exec = async (command) => new Promise((resolve, reject) => {
	childProcess.exec(command, (err, stdout, stderr) => {
		if (err) {
			console.error(stderr);
			return reject(err);
		}
		return resolve(err);
	});
});

module.exports.download = async () => {
	const defaultUrl = "https://www.dropbox.com/s/wdd35wbnhz6du0m/default.jpg";

	const images = fs.readdirSync(path.join(__dirname, "../data/recipes"))
		.map((file) => require(path.join(__dirname, "../data/recipes/") + file))
		.filter(file => !!file.image)
		.map(file => file.image);
	images.push(defaultUrl);

	try {
		await exec("rm -rf images");
		await exec("mkdir images");
		for (let image of images) {
			await exec(`./img.sh ${image} ${path.basename(image)}`);
		}
	} catch (err) {
		return Promise.reject(err);
	}
};

module.exports.clean = () => {
	return gulp.src(["images"], { read: false })
		.pipe(clean());
};
