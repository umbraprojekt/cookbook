const gulp = require("gulp");
const sass = require("gulp-sass");
const rename = require("gulp-rename");
const autoprefix = require("gulp-autoprefixer");
const cleanCSS = require("gulp-clean-css");

module.exports.dev = () => {
	return gulp.src("./scss/index.scss")
		.pipe(sass())
		.pipe(autoprefix({
			cascade: false
		}))
		.pipe(rename("style.css"))
		.pipe(gulp.dest("./public/css"));
};

module.exports.dist = () => {
	return gulp.src("./scss/index.scss")
		.pipe(sass())
		.pipe(autoprefix({
			cascade: false
		}))
		.pipe(cleanCSS())
		.pipe(rename("style.css"))
		.pipe(gulp.dest("./public/css"));
};
