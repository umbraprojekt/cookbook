const gulp = require("gulp");

module.exports = () => {
	return gulp.src("node_modules/@fortawesome/fontawesome-free/webfonts/*")
		.pipe(gulp.dest("public/fonts"));
};
