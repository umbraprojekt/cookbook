const gulp = require("gulp");
const clean = require("./gulp/clean");
const css = require("./gulp/css");
const fonts = require("./gulp/fonts");
const img = require("./gulp/img");
const ts = require("./gulp/ts");
const tslint = require("./gulp/tslint");

gulp.task("clean:dev", clean);
gulp.task("css:dev", css.dev);
gulp.task("css:dist", css.dist);
gulp.task("img:dev", img.dev);
gulp.task("img:dist", img.dist);
gulp.task("img:download", img.download);
gulp.task("img:clean", img.clean);
gulp.task("fonts", fonts);
gulp.task("ts:dev", ts.dev);
gulp.task("ts:dist", ts.dist);
gulp.task("tslint", tslint);

gulp.task("img", gulp.series("img:download", "img:dev", "img:clean"));

gulp.task("dev", gulp.parallel(
	"ts:dev",
	"css:dev",
	"fonts",
	gulp.series("img:download", "img:dev", "img:clean")
));

gulp.task("default", gulp.parallel(
	"ts:dist",
	"css:dist",
	"fonts",
	gulp.series("img:download", "img:dist", "img:clean")
));

gulp.task("watch", () => {
	gulp.watch(["scss/**/*.scss"], gulp.parallel("css:dev"));
	gulp.watch(["**/*.ts"], gulp.series("tslint", "ts:dev"));
});
