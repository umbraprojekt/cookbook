import * as express from "express";
import { NextFunction, Request, Response, Router } from "express";
import {recipeService} from "../services/RecipeService";
import {nunjucks} from "../services/nunjucks";
import {HTTPError} from "../errors/HTTPError";
import {IRecipe} from "../types/IRecipe";
import * as qs from "query-string";
import {Paginator} from "../services/PaginationService";

export const recipeController: Router = express.Router();

recipeController.get("/", (req: Request, res: Response, next: NextFunction) => {
	const q: string = req.query.q as string || "";
	const recipeTags: Array<string> = req.query.tags ?
		(Array.isArray(req.query.tags) ? req.query.tags as Array<string> : [req.query.tags as string]) :
		[];
	const ingredientTags: Array<string> = req.query.ingredients ?
		(Array.isArray(req.query.ingredients) ? req.query.ingredients as Array<string> : [req.query.ingredients as string]) :
		[];

	res.locals.tags = recipeService.getRecipeTags();
	res.locals.ingredients = recipeService.getIngredientTags();
	res.locals.query = req.query;
	res.locals.qs = qs.stringify(req.query as any);
	res.locals.recipes = new Paginator(recipeService.findByAny(q, recipeTags, ingredientTags))
		.withPage(parseInt(req.query.page as string || "1", 10))
		.build();

	return res.send(nunjucks.render("pages/index.njk", res.locals));
});

recipeController.get("/:id", (req: Request, res: Response, next: NextFunction) => {
	const recipe: IRecipe = res.locals.recipe = recipeService.getById(req.params.id);
	if (!recipe) {
		return next(new HTTPError("Recipe not found.", 404));
	}

	return res.send(nunjucks.render("pages/recipe.njk", res.locals));
});
