import * as express from "express";
import * as bodyParser from "body-parser";
import { environment } from "./args";
import { NextFunction, Request, Response } from "express";
import { HTTPError } from "./errors/HTTPError";
import { nunjucks } from "./services/nunjucks";
import * as path from "path";

// controllers
import { recipeController } from "./controllers/recipeController";

export const app = express();

// configure app
app.disable("x-powered-by");
app.use(express.static(path.join(__dirname, "../public")));
app.use(bodyParser.json());
app.use((req: Request, res: Response, next: NextFunction) => {
	res.locals.copyrightYear = new Date().getFullYear();
	next();
});

// controllers
app.use("/", recipeController);

// catch 404 and forward to error handler
app.use((req: Request, res: Response, next: NextFunction) => {
	next(new HTTPError("Not Found", 404));
});

// error handler
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = environment === "development" ? err : {};

	const status: number = err.status || 500;
	res.status(status);

	const page = nunjucks.render("pages/error.njk", res.locals);
	res.send(page);
});
