export interface IRecipeIngredient {
	label: string;
	search: string;
	tags?: Array<string>;
	ref?: string;
}
