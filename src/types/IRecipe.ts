import {IRecipePart} from "./IRecipePart";

export interface IRecipe {
	id: string;
	image?: string;
	title: string;
	author: string;
	difficulty: number;
	time: string;
	servings: number;
	tags: Array<string>;
	parts: Array<IRecipePart>;
}
