export interface IIngredientDescription {
	id: string;
	search: string;
	tags?: Array<string>;
	ref?: string;
}
