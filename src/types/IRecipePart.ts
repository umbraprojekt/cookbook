import {IRecipeIngredient} from "./IRecipeIngredient";

export interface IRecipePart {
	ingredients: Array<IRecipeIngredient>;
	steps: Array<string>;
}
