const alphabet: string = "AĄBCĆDEĘFGHIJKLŁMNŃOÓPQRSŚTUVWXYZŹŻ";

export const localeCompare = (inputA: string, inputB: string): number => {
	let a: number = 0;
	let b: number = 0;
	let i: number = 0;

	while (a === b) {
		a = inputA.length > i ? alphabet.indexOf(inputA[i].toUpperCase()) : -2;
		b = inputB.length > i ? alphabet.indexOf(inputB[i].toUpperCase()) : -2;
		i++;
	}

	return a - b;
};
