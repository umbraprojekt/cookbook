import {Server} from "http";
import * as debug from "debug";

import {app} from "./app";
import * as http from "http";
import {port} from "./args";

const log: debug.IDebugger = debug("cookbook:startup");
const server: Server = http.createServer(app);

server.listen(port);
server.on("error", onError);
server.on("listening", onListening);

process.on("unhandledRejection", (up: any) => {
	throw up;
});

function onError(error: any) {
	if (error.syscall !== "listen") {
		throw error;
	}

	const bind = `Port ${port}`;

	// handle specific listen errors with friendly messages
	switch (error.code) {
		case "EACCES":
			log(bind + " requires elevated privileges");
			process.exit(1);
			break;
		case "EADDRINUSE":
			log(bind + " is already in use");
			process.exit(1);
			break;
		default:
			throw error;
	}
}

function onListening() {
	const addr = server.address();
	const bind = typeof addr === "string"
		? "pipe " + addr
		: "port " + addr.port;
	log("Listening on " + bind);
}
