import * as extend from "extend";
import * as marked from "marked";
import * as format from "number-format.js";
import * as _nunjucks from "nunjucks";
import * as path from "path";
import * as qs from "query-string";

export const nunjucks = new _nunjucks.Environment(
	new _nunjucks.FileSystemLoader(path.join(__dirname, "../../views")),
	{autoescape: true}
);

nunjucks.addFilter("pl", (input: string) => {
	if (typeof input === "number") {
		return format("# ###,#", input, {});
	}
	return input;
});

nunjucks.addFilter("unit", (input: string|number, suffix: string) => {
	if (!input || input === "?" || input === "-") {
		return input;
	}
	return input + suffix;
});

nunjucks.addFilter("qs", (input: any, replacements: any) => {
	const parsed1: any = qs.parse(input);
	const parsed2: any = qs.parse(replacements);
	const joined = extend(true, {}, parsed1, parsed2);
	return new _nunjucks.runtime.SafeString(qs.stringify(joined));
});

nunjucks.addFilter("markdown", (input: any) => {
	return new _nunjucks.runtime.SafeString(marked(input));
});
