import {IPaginatorResponse} from "../responses/IPaginatorResponse";

export class Paginator {
	private _items: Array<any>;
	private _itemsPerPage: number = 24;
	private _page: number = 1;

	constructor(items: Array<any>) {
		this._items = items;
	}

	public withItemsPerPage(itemsPerPage: number): Paginator {
		this._itemsPerPage = itemsPerPage;
		return this;
	}

	public withPage(page: number): Paginator {
		this._page = page;
		return this;
	}

	public build(): IPaginatorResponse {
		return {
			pages: Math.ceil(this._items.length / this._itemsPerPage),
			page: this._page,
			items: this._items.slice((this._page - 1) * this._itemsPerPage, this._page * this._itemsPerPage)
		};
	}
}
