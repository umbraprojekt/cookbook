import * as fs from "fs";
import * as path from "path";
import {localeCompare} from "../utils/localeCompare";
import {IRecipe} from "../types/IRecipe";
import {IRecipePart} from "../types/IRecipePart";
import {IRecipeIngredient} from "../types/IRecipeIngredient";
import { ingredientService, IngredientService } from "./IngredientService";
import * as debug from "debug";
import { IIngredientDescription } from "../types/IIngredientDescription";

const DIRECTORY = path.join(__dirname, "../../data/recipes/");
const log: debug.IDebugger = debug("cookbook:RecipeService");

export class RecipeService {
	private readonly _recipes: Array<IRecipe>;
	private readonly _recipeTags: Array<string>;
	private readonly _ingredientTags: Array<string>;

	constructor(_ingredientService: IngredientService) {
		this._recipes = fs.readdirSync(DIRECTORY)
			.map((file: string) => {
				return require(DIRECTORY + file) as any;
			})
			.map((recipe: any) => {
				recipe.parts.forEach((part: any) => {
					part.ingredients = part.ingredients.map((ingredient: any) => {
						if (ingredient.id === undefined) {
							log(`Undefined ingredient ID. Recipe ID: "${recipe.id}"; ingredient: ${JSON.stringify(ingredient)}.`);
							throw new Error(`Undefined ingredient ID.`);
						}

						let ingredientDescription: IIngredientDescription;
						try {
							ingredientDescription = _ingredientService.find(ingredient.id);
						} catch (e) {
							log(`Unrecognised ingredient in recipe ID: "${recipe.id}".`);
							throw e;
						}

						return {
							...ingredient,
							...ingredientDescription
						};
					});
				});
				return recipe as IRecipe;
			})
			.sort((a: IRecipe, b: IRecipe) => localeCompare(a.title, b.title));

		this._recipeTags = [];
		this._recipes.map((recipe: IRecipe) => recipe.tags.forEach((tag: string) => {
			if (this._recipeTags.indexOf(tag) === -1) {
				this._recipeTags.push(tag);
			}
		}));
		this._recipeTags.sort(localeCompare);

		this._ingredientTags = [];
		this._recipes.map((recipe: IRecipe) => recipe.parts.forEach((part: IRecipePart) => part.ingredients
			.forEach((ingredient: IRecipeIngredient) => {
				if (ingredient.tags) {
					ingredient.tags.forEach((tag: string) => {
						if (this._ingredientTags.indexOf(tag) === -1) {
							this._ingredientTags.push(tag);
						}
					});
				}
			})));
		this._ingredientTags.sort(localeCompare);
	}

	public getAll(): Array<IRecipe> {
		return this._recipes;
	}

	public getById(id: string): IRecipe {
		return this._recipes.find((recipe: IRecipe) => recipe.id === id);
	}

	public findByAny(input: string, tags: Array<string>, ingredients: Array<string>): Array<IRecipe> {
		let results: Array<IRecipe> = this._recipes;

		if (tags.length) {
			results = results.filter((recipe: IRecipe) => tags.every((tag: string) => recipe.tags.indexOf(tag) > -1));
		}

		if (ingredients.length) {
			results = results.filter((recipe: IRecipe) => ingredients.every((tag: string) => {
				return recipe.parts.some((part: IRecipePart) => part.ingredients
					.some((ingredient: IRecipeIngredient) => ingredient.tags && ingredient.tags.indexOf(tag) > -1));
			}));
		}

		if (input.length) {
			const normInput: string = input.toLowerCase();
			return results.filter((recipe: IRecipe) => {
				return recipe.title.toLowerCase().includes(normInput)
					|| recipe.tags.some((tag: string) => tag.includes(normInput))
					|| recipe.parts.some((part: IRecipePart) => part.ingredients
						.some((ingredient: IRecipeIngredient) => ingredient.search.toLowerCase().includes(normInput)));
			});
		}

		return results;
	}

	public getRecipeTags(): Array<string> {
		return this._recipeTags;
	}

	public getIngredientTags(): Array<string> {
		return this._ingredientTags;
	}
}

export const recipeService: RecipeService = new RecipeService(ingredientService);
