import * as fs from "fs";
import * as path from "path";
import * as debug from "debug";
import { IIngredientDescription } from "../types/IIngredientDescription";

const DIRECTORY = path.join(__dirname, "../../data/ingredients/");

const log: debug.IDebugger = debug("cookbook:IngredientService");

export class IngredientService {
	private _ingredients: Map<string, IIngredientDescription> = new Map();

	public constructor() {
		fs.readdirSync(DIRECTORY)
			.map((file: string) => {
				return {
					id: path.basename(file, ".json"),
					...require(DIRECTORY + file)
				} as IIngredientDescription;
			})
			.forEach((ingredient: IIngredientDescription) => {
				this._ingredients.set(ingredient.id, ingredient);
			});
	}

	public find(id: string|Array<string>): IIngredientDescription {
		const search: Array<string> = Array.isArray(id) ? id : [id];
		return search
			.map(item => {
				if (!this._ingredients.has(item)) {
					log(`Unrecognised ingredient ID: "${item}".`);
					throw new Error(`Unrecognised ingredient ID: "${item}".`);
				}
				return this._ingredients.get(item);
			})
			.reduce((prev: IIngredientDescription, cur: IIngredientDescription) => {
				prev.id = prev.id ? cur.id : "," + cur.id;
				prev.search = prev.search ? cur.search : " " + cur.search;
				if (cur.ref) {
					prev.ref = cur.ref;
				}
				if (cur.tags) {
					prev.tags = [...new Set(prev.tags ? prev.tags.concat(cur.tags) : cur.tags)];
				}
				return prev;
			}, {id: "", search: ""});
	}
}

export const ingredientService = new IngredientService();
