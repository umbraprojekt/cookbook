export interface IPaginatorResponse {
	pages: number;
	page: number;
	items: Array<any>;
}
