# My Recipes

Run `npm install` after cloning the repo.

Available `npm` commands:

* `npm run dev` - build for development
* `npm run dist` - build for production
* `npm test` - run tests
* `npm start` - run the dev application
* `npm run watch` - enable watch mode (automatically rebuild and run tests after a change is detected in either source or test code)

In order to run the production app, build it and run `node dist/index.js -e production`.

## Application command line arguments

You can control the application config using command line arguments when running the app.

* `-e` or `--env` - choose running environment. Defaults to `development`.
* `-p` or `--port` - set the server port. Defaults to `3000`.
* `-h` or `--host` - set the server hostname. Defaults to `127.0.0.1`.

## Environment variables

You can also control the application config using environment variables. They always take precedence before any command line arguments.

* `NODE_ENV` - choose running environment.
* `PORT` - set server port
